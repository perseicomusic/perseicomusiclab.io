var lang = (navigator.language || navigator.userLanguage).substr(0, 2);

switch (lang) {
  case 'pt':
    loadTranslation('pt');
    break;
  case 'es':
    loadTranslation('es');
    break;
}

function loadTranslation(code) {
  var request = new XMLHttpRequest();

  request.onreadystatechange = function () {
    if (this.readyState == 4) {
      if (this.status == 200) {
        applyTranslation(JSON.parse(this.responseText));
      } else {
        throw new Error("Translation failed");
      }
    }
  };

  request.open('GET', 'trans/' + code + '.json', true);
  request.send();
}

function applyTranslation(obj) {
  for (var key in obj) {
    let elems = document.getElementsByClassName(key);

    for (var i = 0; i < elems.length; i++) {
      elems[i].textContent = obj[key];
    }
  }
}
